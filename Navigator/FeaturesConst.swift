//
//  FeaturesConst.swift
//  POCCAIXA+
//
//  Created by gemed on 06/05/2019.
//  Copyright © 2019 gemed. All rights reserved.
//


import UIKit
import Transferencia

public enum StoryboardNames: String {
    case transferencia = "Transferencia"
}
public class FeaturesConst: NSObject {
    
    
    public static func get(_ feature: StoryboardNames) -> UIViewController? {
        var storyboard: UIStoryboard
        
        switch feature {
        case .transferencia:
            storyboard = UIStoryboard(name: feature.rawValue, bundle: Bundle(for: TransferenciaViewController.self))
            
        default:
            return nil
        }
        
        guard let vc = storyboard.instantiateInitialViewController() else {
            return nil
        }
        return vc
    }
}
